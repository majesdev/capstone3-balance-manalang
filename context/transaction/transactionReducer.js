import {
  TRANSACTION_REQUEST,
  TRANSACTION_REQUEST_FAIL,
  ADD_TRANSACTION,
  ACCUMULATE_TOTAL,
  GET_TRANSACTIONS,
} from './transactionTypes';

const transactionReducer = (state, action) => {
  switch (action.type) {
    case TRANSACTION_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_TRANSACTIONS:
      return {
        ...state,
        transactions: action.payload.transactions,
        loading: false,
        error: null,
      };

    case ADD_TRANSACTION:
      return {
        ...state,
        transactions: [action.payload.transaction, ...state.transactions],
        loading: false,
        error: null,
      };
    case ACCUMULATE_TOTAL:
      return {
        ...state,
        total:
          state.transactions.length > 0
            ? state.transactions
                .map(transaction => transaction.amount)
                .reduce((acc, val) => acc + val)
            : 0,
      };

    case TRANSACTION_REQUEST_FAIL:
      return {
        ...state,
        error: action.payload.error,
      };
  }
};

export default transactionReducer;
