import React, { createContext, useReducer } from 'react';
import categoryReducer from './categoryReducer';
import axios from '../../axios';
import {
  CATEGORY_REQUEST,
  CATEGORY_REQUEST_FAIL,
  ADD_CATEGORY,
  UPDATE_CATEGORY,
  DELETE_CATEGORY,
  GET_CATEGORIES,
} from './categoryTypes';

const CategoryContext = createContext();

export const CategoryProvider = ({ children }) => {
  const initialState = {
    categories: [],
    loading: false,
    error: null,
  };

  const [state, dispatch] = useReducer(categoryReducer, initialState);

  const addCategory = async category => {
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };
    try {
      dispatch({ type: CATEGORY_REQUEST });
      const { data } = await axios.post('/categories', category, config);
      dispatch({ type: ADD_CATEGORY, payload: data });
    } catch (error) {
      console.log(error.response.data);
      dispatch({ type: CATEGORY_REQUEST_FAIL, payload: error.response.data });
    }
  };

  const fetchCategories = async () => {
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    };
    try {
      dispatch({ type: CATEGORY_REQUEST });
      const { data } = await axios.get('/categories', config);
      dispatch({ type: GET_CATEGORIES, payload: data });
    } catch (error) {
      console.log(error.response.data);
      dispatch({ type: CATEGORY_REQUEST_FAIL, payload: error.response.data });
    }
  };

  return (
    <CategoryContext.Provider
      value={{
        ...state,
        categoryLoading: state.loading,
        categoryDispatch: dispatch,
        addCategory,
        fetchCategories,
      }}
    >
      {children}
    </CategoryContext.Provider>
  );
};

export default CategoryContext;
