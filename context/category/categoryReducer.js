import {
  CATEGORY_REQUEST,
  CATEGORY_REQUEST_FAIL,
  GET_CATEGORIES,
  ADD_CATEGORY,
  UPDATE_CATEGORY,
  DELETE_CATEGORY,
} from './categoryTypes';

const categoryReducer = (state, action) => {
  switch (action.type) {
    case CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_CATEGORIES:
      return {
        ...state,
        categories: action.payload.categories,
        loading: false,
        error: null,
      };

    case ADD_CATEGORY:
      return {
        ...state,
        categories: [...state.categories, action.payload.category],
        loading: false,
        error: null,
      };

    case UPDATE_CATEGORY:
      return {
        ...state,
        categories: state.categories.map(category =>
          category._id === action.payload.category._id
            ? action.payload.category
            : category
        ),
        loading: false,
        error: null,
      };

    case DELETE_CATEGORY:
      return {
        ...state,
        categories: state.categories.filter(
          category => category._id !== action.payload.category._id
        ),
        loading: false,
        error: null,
      };
    case CATEGORY_REQUEST_FAIL:
      return {
        ...state,
        error: action.payload.error,
      };
  }
};

export default categoryReducer;
