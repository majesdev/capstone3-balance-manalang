import React, { useState } from 'react';
import TransactionItem from './TransactionItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';

const TransactionList = ({ transactions }) => {
  const [transactionType, setTransactionType] = useState('All');
  const [search, setSearch] = useState('');

  const allTransactions = transactions
    .filter(transaction => {
      const regex = RegExp(`${search}`, 'gi');
      return transaction.description.match(regex);
    })
    .map(transaction => (
      <TransactionItem
        key={transaction._id}
        createdOn={new Date(transaction.createdOn).toLocaleDateString('en-US')}
        description={transaction.description}
        amount={transaction.amount}
        category={transaction.category}
        type={transaction.type}
      />
    ));

  const filteredTransactions = transactions
    .filter(transaction => transaction.type === transactionType)
    .filter(transaction => {
      const regex = RegExp(`${search}`, 'gi');
      return transaction.description.match(regex);
    })
    .map(transaction => (
      <TransactionItem
        key={transaction._id}
        createdOn={new Date(transaction.createdOn).toLocaleDateString('en-US')}
        description={transaction.description}
        amount={transaction.amount}
        category={transaction.category}
        type={transaction.type}
      />
    ));

  return (
    <div>
      <Box display="flex" justifyContent="space-between">
        <TextField
          style={{ marginRight: '1rem' }}
          fullWidth
          label="Search"
          variant="outlined"
          onChange={e => setSearch(e.target.value)}
          value={search}
          name="search"
          type="text"
        />
        <FormControl fullWidth variant="outlined">
          <InputLabel id="transactionTypeLabel">Filter Transaction</InputLabel>
          <Select
            labelId="transactionTypeLabel"
            id="transactionType"
            value={transactionType}
            onChange={e => setTransactionType(e.target.value)}
            label="Filter Transaction"
          >
            <MenuItem value="All">All Transactions</MenuItem>
            <MenuItem value="Income">Income</MenuItem>
            <MenuItem value="Expense">Expense</MenuItem>
          </Select>
        </FormControl>
      </Box>
      {transactionType === 'All' ? allTransactions : filteredTransactions}
    </div>
  );
};

export default TransactionList;
