import React from 'react';
import CategoryItem from './CategoryItem';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Tag from '../../styles/Tag';

const useStyles = makeStyles({
  table: {
    width: '100%',
  },
});

const CategoryList = ({ categories }) => {
  const classes = useStyles();
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Category Name</TableCell>
            <TableCell align="right">Category Type</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {categories?.map(category => (
            <TableRow key={category.name}>
              <TableCell component="th" scope="row">
                {category?.name}
              </TableCell>
              <TableCell align="right">
                <Tag
                  style={{ marginLeft: 'auto' }}
                  isIncome={category.type === 'Income' ? true : false}
                >
                  {category?.type}
                </Tag>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default CategoryList;
