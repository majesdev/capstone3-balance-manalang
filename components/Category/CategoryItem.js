import React from 'react';
import Card from '../../styles/Card';
import Tag from '../../styles/Tag';
import styled from 'styled-components';

const CategoryCard = styled(Card)`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
`;

const CategoryItem = ({ category }) => {
  return (
    <CategoryCard>
      <h1 className="mid-heading">{category.name}</h1>
      <Tag isIncome={category.type === 'Income' ? true : false}>
        {category.type}
      </Tag>
    </CategoryCard>
  );
};

export default CategoryItem;
