import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Button from '../../styles/Button';
import styled from 'styled-components';

const TextFieldWrapper = styled.div`
  flex: 1;
  margin-right: 1.25rem;
`;

const FormControlWrapper = styled.div`
  /* flex: 1; */
`;

const CategoryForm = ({ addCategory }) => {
  const [categoryName, setCategoryName] = useState('');
  const [categoryType, setCategoryType] = useState('Income');

  const handleSubmit = e => {
    e.preventDefault();
    console.log(categoryName);
    console.log(categoryType);
    addCategory({
      name: categoryName,
      type: categoryType,
    });

    setCategoryName('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <Box display="flex" justifyContent="space-between">
        <TextFieldWrapper>
          <TextField
            fullWidth
            m={2}
            key="categoryName"
            variant="outlined"
            label="Category Name"
            onChange={e => setCategoryName(e.target.value)}
            value={categoryName}
            name="categoryName"
            type="text"
            placeholder="Salary, Meralco, Netflix, etc. "
          />
        </TextFieldWrapper>
        <FormControlWrapper>
          <FormControl variant="outlined">
            <InputLabel id="categoryTypeLabel">Type</InputLabel>
            <Select
              labelId="categoryTypeLabel"
              id="categoryType"
              value={categoryType}
              onChange={e => setCategoryType(e.target.value)}
              label="Type"
            >
              <MenuItem value="Income">Income</MenuItem>
              <MenuItem value="Expense">Expense</MenuItem>
            </Select>
          </FormControl>
        </FormControlWrapper>
      </Box>
      <Button type="submit">Add Category</Button>
    </form>
  );
};

export default CategoryForm;
