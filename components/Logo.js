import React from 'react';
import { useRouter } from 'next/router';
import theme from '../styles/theme';
import styled from 'styled-components';

const { colors, fonts, fontSizes } = theme;

const StyledLogo = styled.h1`
  font-family: ${fonts.Calibre};
  font-size: ${fontSizes.xxl};
  font-weight: bold;
  color: ${props => props.color || colors.purple};
  cursor: pointer;

  @media screen and (max-width: 964px) {
    display: none;
  }
`;

const Logo = () => {
  const router = useRouter();
  return <StyledLogo onClick={e => router.push('/')}>Balance.</StyledLogo>;
};

export default Logo;
