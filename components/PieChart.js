import React, { useEffect, useState } from 'react';
import { Pie } from 'react-chartjs-2';
import theme from '../styles/theme';

const { colors } = theme;

const PieChart = ({ categories, transactions }) => {
  const [type, setType] = useState(new Array());
  const [description, setDescription] = useState(new Array());
  const [amount, setAmount] = useState(new Array());
  const [color, setColor] = useState(new Array());

  useEffect(() => {
    setType(transactions.map(element => element.type));
    setDescription(transactions.map(element => element.description));
    setAmount(transactions.map(element => element.amount));
    setColor(
      transactions.map(element =>
        element.type === 'Income' ? colors.green : colors.yellow
      )
    );
  }, [transactions]);

  const data = {
    labels: description,
    datasets: [
      {
        data: amount,
        backgroundColor: color,
      },
    ],
  };

  return (
    <React.Fragment>
      <Pie data={data} />
    </React.Fragment>
  );
};

export default PieChart;
