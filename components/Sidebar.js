import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';
import MenuIcon from '@material-ui/icons/Menu';
import theme from '../styles/theme';
import Logo from './Logo';
import AuthContext from '../context/auth/authContext';

const { colors } = theme;

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    backgroundColor: colors.black,
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    flexShrink: 0,
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  toolbar: {
    ...theme.mixins.toolbar,
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
}));

export default function Sidebar() {
  const { state, logout } = useContext(AuthContext);

  const router = useRouter();
  const classes = useStyles();
  const theme = useTheme();
  const isMdUp = useMediaQuery(theme.breakpoints.up('md'));

  const navLinks = [
    { text: 'Dashboard', link: '/', icon: 'dashboard' },
    { text: 'Categories', link: '/categories', icon: 'view_list' },
    { text: 'Transactions', link: '/transactions', icon: 'account_balance' },
    {
      text: 'Breakdown',
      link: '/categories/breakdown',
      icon: 'group_work',
    },
    { text: 'Monthly Income', link: '/monthly/income', icon: 'trending_up' },
    {
      text: 'Monthly Expense',
      link: '/monthly/expense',
      icon: 'trending_down',
    },
    { text: 'Budget Trend', link: '/budget-trend', icon: 'monetization_on' },
    { text: 'Profile', link: '/profile', icon: 'account_box' },
  ];

  const [open, setOpen] = React.useState(false);

  const toggleDrawer = event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setOpen(!open);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={toggleDrawer}
            className={classes.menuButton}
          >
            <MenuIcon color="primary" />
          </IconButton>
          <Logo />
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant={isMdUp ? 'permanent' : 'temporary'}
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
        open={open}
        onClose={toggleDrawer}
      >
        <div className={classes.toolbar} />
        <Divider />
        <List>
          {navLinks.map(navLink => (
            <ListItem
              onClick={() => router.push(navLink.link)}
              button
              key={navLink.text}
            >
              <ListItemIcon>
                <Icon>{navLink.icon}</Icon>
              </ListItemIcon>
              <ListItemText primary={navLink.text} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          <ListItem onClick={() => router.push('/about')} button>
            <ListItemIcon>
              <Icon>info</Icon>
            </ListItemIcon>
            <ListItemText primary="About" />
          </ListItem>
          <ListItem onClick={logout} button>
            <ListItemIcon>
              <Icon>logout</Icon>
            </ListItemIcon>
            <ListItemText primary="Logout" />
          </ListItem>
        </List>
      </Drawer>
    </React.Fragment>
  );
}
