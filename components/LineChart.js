import { useState, useEffect, Fragment } from 'react';
import { Line } from 'react-chartjs-2';
import theme from '../styles/theme';
const { colors } = theme;

const LineChart = ({ rawData }) => {
  const [months, setMonths] = useState([
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ]);
  const [amount, setAmount] = useState([]);

  useEffect(() => {
    console.log(rawData);

    setAmount(rawData.map(transaction => Math.abs(transaction.amount)));
  }, [rawData]);

  const data = {
    labels: 'dateRange' in rawData ? rawData.dateRange : months,
    datasets: [
      {
        label: 'Budget Trend',
        borderColor: colors.purple,
        borderWidth: 3,
        hoverBorderColor: 'black',
        data: amount,
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return (
    <Fragment>
      <Line data={data} options={options} />
    </Fragment>
  );
};

export default LineChart;
