import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import styled from 'styled-components';

const SpinnerWrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Spinner = () => (
  <SpinnerWrapper>
    <CircularProgress />
  </SpinnerWrapper>
);

export default Spinner;
