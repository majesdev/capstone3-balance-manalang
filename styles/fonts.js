import { css } from 'styled-components';

import CalibreLightTTF from '../public/fonts/Calibre/Calibre-Light.ttf';
import CalibreLightWOFF from '../public/fonts/Calibre/Calibre-Light.woff';
import CalibreLightWOFF2 from '../public/fonts/Calibre/Calibre-Light.woff2';
import CalibreLightItalicTTF from '../public/fonts/Calibre/Calibre-LightItalic.ttf';
import CalibreLightItalicWOFF from '../public/fonts/Calibre/Calibre-LightItalic.woff';
import CalibreLightItalicWOFF2 from '../public/fonts/Calibre/Calibre-LightItalic.woff2';
import CalibreRegularTTF from '../public/fonts/Calibre/Calibre-Regular.ttf';
import CalibreRegularWOFF from '../public/fonts/Calibre/Calibre-Regular.woff';
import CalibreRegularWOFF2 from '../public/fonts/Calibre/Calibre-Regular.woff2';
import CalibreRegularItalicTTF from '../public/fonts/Calibre/Calibre-RegularItalic.ttf';
import CalibreRegularItalicWOFF from '../public/fonts/Calibre/Calibre-RegularItalic.woff';
import CalibreRegularItalicWOFF2 from '../public/fonts/Calibre/Calibre-RegularItalic.woff2';
import CalibreMediumTTF from '../public/fonts/Calibre/Calibre-Medium.ttf';
import CalibreMediumWOFF from '../public/fonts/Calibre/Calibre-Medium.woff';
import CalibreMediumWOFF2 from '../public/fonts/Calibre/Calibre-Medium.woff2';
import CalibreMediumItalicTTF from '../public/fonts/Calibre/Calibre-MediumItalic.ttf';
import CalibreMediumItalicWOFF from '../public/fonts/Calibre/Calibre-MediumItalic.woff';
import CalibreMediumItalicWOFF2 from '../public/fonts/Calibre/Calibre-MediumItalic.woff2';
import CalibreSemiboldTTF from '../public/fonts/Calibre/Calibre-Semibold.ttf';
import CalibreSemiboldWOFF from '../public/fonts/Calibre/Calibre-Semibold.woff';
import CalibreSemiboldWOFF2 from '../public/fonts/Calibre/Calibre-Semibold.woff2';
import CalibreSemiboldItalicTTF from '../public/fonts/Calibre/Calibre-SemiboldItalic.ttf';
import CalibreSemiboldItalicWOFF from '../public/fonts/Calibre/Calibre-SemiboldItalic.woff';
import CalibreSemiboldItalicWOFF2 from '../public/fonts/Calibre/Calibre-SemiboldItalic.woff2';

const FontFaces = css`
  @font-face {
    font-family: 'Calibre';
    src: url(${CalibreLightWOFF2}) format('woff2'),
      url(${CalibreLightWOFF}) format('woff'),
      url(${CalibreLightTTF}) format('truetype');
    font-weight: 300;
    font-style: normal;
  }
  @font-face {
    font-family: 'Calibre';
    src: url(${CalibreLightItalicWOFF2}) format('woff2'),
      url(${CalibreLightItalicWOFF}) format('woff'),
      url(${CalibreLightItalicTTF}) format('truetype');
    font-weight: 300;
    font-style: italic;
  }
  @font-face {
    font-family: 'Calibre';
    src: url(${CalibreRegularWOFF2}) format('woff2'),
      url(${CalibreRegularWOFF}) format('woff'),
      url(${CalibreRegularTTF}) format('truetype');
    font-weight: normal;
    font-style: normal;
  }
  @font-face {
    font-family: 'Calibre';
    src: url(${CalibreRegularItalicWOFF2}) format('woff2'),
      url(${CalibreRegularItalicWOFF}) format('woff'),
      url(${CalibreRegularItalicTTF}) format('truetype');
    font-weight: normal;
    font-style: italic;
  }
  @font-face {
    font-family: 'Calibre';
    src: url(${CalibreMediumWOFF2}) format('woff2'),
      url(${CalibreMediumWOFF}) format('woff'),
      url(${CalibreMediumTTF}) format('truetype');
    font-weight: 500;
    font-style: normal;
  }
  @font-face {
    font-family: 'Calibre';
    src: url(${CalibreMediumItalicWOFF2}) format('woff2'),
      url(${CalibreMediumItalicWOFF}) format('woff'),
      url(${CalibreMediumItalicTTF}) format('truetype');
    font-weight: 500;
    font-style: italic;
  }
  @font-face {
    font-family: 'Calibre';
    src: url(${CalibreSemiboldWOFF2}) format('woff2'),
      url(${CalibreSemiboldWOFF}) format('woff'),
      url(${CalibreSemiboldTTF}) format('truetype');
    font-weight: 600;
    font-style: normal;
  }
  @font-face {
    font-family: 'Calibre';
    src: url(${CalibreSemiboldItalicWOFF2}) format('woff2'),
      url(${CalibreSemiboldItalicWOFF}) format('woff'),
      url(${CalibreSemiboldItalicTTF}) format('truetype');
    font-weight: 600;
    font-style: italic;
  }
`;

export default FontFaces;
