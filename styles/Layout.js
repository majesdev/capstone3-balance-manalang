import styled from 'styled-components';
import theme from '../styles/theme';
const { colors, fonts, fontSizes } = theme;

const Layout = styled.div`
  display: flex;
  margin-top: 5rem;
`;

export default Layout;
