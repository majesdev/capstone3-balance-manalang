import { createGlobalStyle } from 'styled-components';
import theme from './theme';
import FontFaces from './fonts';
const { colors, fonts, fontSizes } = theme;

const GlobalStyle = createGlobalStyle`
  ${FontFaces};

  *,
  *:before,
  *:after {
    box-sizing: inherit;
    margin: 0;
    padding: 0;
  }

  html {
    box-sizing: border-box;
  }

  body {
    font-family: ${fonts.Calibre};
    font-size: ${fontSizes.md};
    line-height: 1.3;
    color: ${colors.blue};
  }

  .container {
    width: 100%;
    max-width: 1200px;
    margin: 0 auto;
    padding: 0 1.5rem;

  }

  .logo {
    color: ${colors.purple};
  }

  .text-center {
    text-align: center;
  }

  .text-white {
    color: #fff;
  }

  .mid-heading {
    font-family: ${fonts.Calibre};
    color: ${colors.navyBlue};
    font-size: 3.6rem;
  }

  .big-heading {
    font-family: ${fonts.Calibre};
    color: ${colors.navyBlue};
    font-size: clamp(30px, 6vw, 60px);
  }

  .mid-heading {
    font-family: ${fonts.Calibre};
    color: ${colors.navyBlue};
    font-size: clamp(25px, 5vw, 50px);
  }

  .subtitle {
    font-family: ${fonts.SFMono};
    font-size: ${fontSizes.md};
  }

  .m-1 {
    margin: 1rem;
  }

  .m-2 {
    margin: 2rem;
  }

  .mr-1 {
    margin-right: 1rem;
  }

  .ml-1 {
    margin-left: 1rem;
  }
`;

export default GlobalStyle;
