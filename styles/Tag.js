import styled from 'styled-components';
import theme from '../styles/theme';
const { colors, fonts, fontSizes } = theme;

const Tag = styled.div`
  background-color: ${props =>
    props.isIncome ? colors.lightGreen : colors.lightYellow};
  color: ${colors.black};
  font-size: ${fontSizes.sm};
  border-radius: 6px;
  padding: 7px;
  border: 1px solid ${props => (props.isIncome ? colors.green : colors.yellow)};
  max-width: 150px;
  text-align: center;
`;

export default Tag;
