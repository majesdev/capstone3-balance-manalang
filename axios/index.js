import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://capstone3-balance-server.herokuapp.com/api/v1',
});

export default instance;
