import React, { useContext, useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import LineChart from '../components/LineChart';
import Spinner from '../components/Spinner';
import Sidebar from '../components/Sidebar';
import Layout from '../styles/Layout';
import AuthContext from '../context/auth/authContext';
import CategoryContext from '../context/category/categoryContext';
import TransactionContext from '../context/transaction/transactionContext';
import { Container, Grid } from '@material-ui/core';
import Card from '../styles/Card';
import styled from 'styled-components';
import theme from '../styles/theme';
import TransactionItem from '../components/Transaction/TransactionItem';
import Icon from '@material-ui/core/Icon';

const { colors, fonts, fontSizes } = theme;

const Count = styled.h2`
  font-family: ${fonts.Calibre};
  font-size: ${fontSizes.xl};
  color: ${colors.purple};
`;

const Description = styled.p`
  font-family: ${fonts.Calibre};
  font-size: ${fontSizes.sm};
  color: ${colors.grey};
`;

const Home = () => {
  const { state, dispatch } = useContext(AuthContext);
  const {
    transactions,
    fetchTransactions,
    transactionLoading,
    total,
  } = useContext(TransactionContext);
  const { categories, fetchCategories, categoryLoading } = useContext(
    CategoryContext
  );
  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem('token') === null) {
      router.push('/login');
    } else {
      fetchCategories();
      fetchTransactions();
    }
    console.log('useeffect from home');
  }, [state]);

  return (
    <React.Fragment>
      <Head>
        <title>Dashboard | Home</title>
      </Head>
      <Layout>
        <Sidebar />
        <Container maxWidth="xl">
          <h1 className="mid-heading">Dashboard</h1>
          <Grid container spacing={3}>
            <Grid item lg={3} sm={6} xs={12}>
              <Card style={{ padding: '2rem 3rem' }}>
                {categoryLoading ? (
                  <Spinner />
                ) : (
                  <>
                    <Count>{categories?.length}</Count>
                    <Description>Total Categories</Description>
                  </>
                )}
              </Card>
            </Grid>
            <Grid item lg={3} sm={6} xs={12}>
              <Card style={{ padding: '2rem 3rem' }}>
                {transactionLoading ? (
                  <Spinner />
                ) : (
                  <>
                    <Count>
                      {transactions?.filter(t => t.type === 'Income').length}
                    </Count>
                    <Description>Income Transactions</Description>
                  </>
                )}
              </Card>
            </Grid>
            <Grid item lg={3} sm={6} xs={12}>
              <Card style={{ padding: '2rem 3rem' }}>
                {transactionLoading ? (
                  <Spinner />
                ) : (
                  <>
                    <Count>
                      {transactions?.filter(t => t.type === 'Expense').length}
                    </Count>
                    <Description>Expense Transactions</Description>
                  </>
                )}
              </Card>
            </Grid>
            <Grid item lg={3} sm={6} xs={12}>
              <Card style={{ padding: '2rem 3rem' }}>
                {transactionLoading ? (
                  <Spinner />
                ) : (
                  <>
                    <Count>&#8369;{total}</Count>
                    <Description>Current Balance</Description>
                  </>
                )}
              </Card>
            </Grid>
          </Grid>
          {/* End */}
          <Grid container spacing={3}>
            <Grid item lg={8} md={6} xs={12}>
              <Card
                onClick={() => router.push('/budget-trend')}
                style={{ padding: '2rem 3rem', cursor: 'pointer' }}
              >
                <LineChart rawData={transactions} />
              </Card>
            </Grid>
            <Grid item lg={4} md={6} xs={12}>
              <Card style={{ maxHeight: '650px' }}>
                <Count>Recent Transactions</Count>
                {transactionLoading ? (
                  <Spinner />
                ) : (
                  <>
                    {transactions.slice(0, 5).map(t => (
                      <TransactionItem
                        key={t._id}
                        createdOn={new Date(t.createdOn).toLocaleDateString(
                          'en-US'
                        )}
                        description={t.description}
                        amount={t.amount}
                        category={t.category}
                        type={t.type}
                      />
                    ))}
                  </>
                )}
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Layout>
    </React.Fragment>
  );
};

export default Home;
