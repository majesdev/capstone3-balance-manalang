import React, { useEffect, useState, useContext } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { TransactionList, TransactionForm } from '../../components/Transaction';
import AuthContext from '../../context/auth/authContext';
import TransactionContext from '../../context/transaction/transactionContext';
import Sidebar from '../../components/Sidebar';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';
import Layout from '../../styles/Layout';
import Card from '../../styles/Card';
import Container from '@material-ui/core/Container';
import Spinner from '../../components/Spinner';

const SectionTitle = styled.h1`
  padding: 1.25rem 0;
  text-align: center;

  @media screen and (max-width: 1200px) {
    color: blue;
  }
`;

const TransactionsPage = () => {
  const { state, dispatch } = useContext(AuthContext);
  const router = useRouter();

  const {
    total,
    transactions,
    fetchTransactions,
    addTransaction,
    loading,
  } = useContext(TransactionContext);
  const [transactionType, setTransactionType] = useState('All');

  useEffect(() => {
    if (localStorage.getItem('token') !== null) {
      fetchTransactions();
    } else {
      router.push('/login');
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Transactions | B.</title>
      </Head>
      <Layout>
        <Sidebar />
        <Container maxWidth="lg">
          <Grid justify="center" container spacing={3}>
            <Grid item md={4} xs={12}>
              <h1 className="mid-heading">Balance:</h1>
              <Card>
                <h1 className="big-heading">&#8369;{total.toLocaleString()}</h1>
              </Card>
              <TransactionForm addTransaction={addTransaction} />
            </Grid>
            <Grid item md={8} xs={12}>
              <h1 className="mid-heading text-center">Transactions History</h1>
              {loading ? (
                <Spinner />
              ) : (
                <TransactionList transactions={transactions} />
              )}
            </Grid>
          </Grid>
        </Container>
      </Layout>
    </React.Fragment>
  );
};

export default TransactionsPage;
