import React, { useEffect, useContext, useState } from 'react';
import NProgress from 'nprogress';
import { useRouter } from 'next/router';
import Router from 'next/router';
import GlobalStyle from '../styles/GlobalStyle';
import { AuthProvider } from '../context/auth/authContext';
import { CategoryProvider } from '../context/category/categoryContext';
import { TransactionProvider } from '../context/transaction/transactionContext';
import Head from 'next/head';

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

const MyApp = ({ Component, pageProps }) => {
  const router = useRouter();

  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css" />
      </Head>
      <AuthProvider>
        <CategoryProvider>
          <TransactionProvider>
            <GlobalStyle />

            {/* {router.pathname !== '/login' && router.pathname !== '/signup' && (
              <Sidebar />
            )} */}
            <Component {...pageProps} />
          </TransactionProvider>
        </CategoryProvider>
      </AuthProvider>
    </React.Fragment>
  );
};

export default MyApp;
