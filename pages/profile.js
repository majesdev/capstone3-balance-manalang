import React, { useEffect, useContext } from 'react';
import AuthContext from '../context/auth/authContext';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Sidebar from '../components/Sidebar';
import styled from 'styled-components';
import theme from '../styles/theme';
import Layout from '../styles/Layout';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

const { colors } = theme;

const Info = styled.div`
  text-align: center;
  background: #fff;
  padding: 2rem 3rem;
  border-radius: 6px;
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
`;

const Title = styled.h1`
  color: ${colors.purple};
  padding: 2rem 0;
  text-align: center;
`;

const ProfilePage = () => {
  const {
    state: { user },
  } = useContext(AuthContext);
  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem('token') === null) {
      router.push('/login');
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Profile | B.</title>
      </Head>
      <Layout>
        <Sidebar />
        <div className="container">
          <Title className="big-heading">Profile Page</Title>
          <Grid container spacing={3}>
            <Grid item sm={12}>
              <Info>
                <h1>
                  {user?.firstName} {user?.lastName}
                </h1>
                <p>{user?.email}</p>
              </Info>
            </Grid>
          </Grid>
        </div>
      </Layout>
    </React.Fragment>
  );
};

export default ProfilePage;
