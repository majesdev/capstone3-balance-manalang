import React, { useEffect, useState, useContext } from 'react';
import Sidebar from '../../components/Sidebar';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { CategoryList, CategoryForm } from '../../components/Category';
import AuthContext from '../../context/auth/authContext';
import CategoryContext from '../../context/category/categoryContext';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';
import Layout from '../../styles/Layout';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import Spinner from '../../components/Spinner';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const SectionTitle = styled.h1`
  padding: 1.25rem 0;
  text-align: center;
`;

const CategoriesPage = () => {
  const { state } = useContext(AuthContext);
  const {
    categories,
    addCategory,
    fetchCategories,
    error,
    loading,
  } = useContext(CategoryContext);
  const router = useRouter();

  useEffect(() => {
    fetchCategories();
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>Categories | B.</title>
      </Head>
      <Layout>
        <Sidebar />
        <Container maxWidth="lg">
          <Grid justify="center" container spacing={3}>
            <Grid item lg={8} md={10} xs={12}>
              {error && <Alert severity="error">{error}</Alert>}
              <SectionTitle>Create Category</SectionTitle>
              <CategoryForm addCategory={addCategory} />
              {loading ? <Spinner /> : <CategoryList categories={categories} />}
            </Grid>
          </Grid>
        </Container>
      </Layout>
    </React.Fragment>
  );
};

export default CategoriesPage;
